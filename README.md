# Test vector generator for hash functions.

[ghash](https://gitlab.com/glatan/ghash)用のテストベクター生成ツール

## BLAKE

ライセンスがどうなってるのかよくわからないのでリファレンス実装のソースコードは同梱していません。

### Round 1/2(BLAKE-{28, 32, 48, 64})

[ここ](https://web.archive.org/web/20160521155743/http://csrc.nist.gov/groups/ST/hash/sha-3/Round2/documents/BLAKE_Round2.zip)からパッケージをダウンロードして`blake_ref.c`と`blake_ref.h`を用意してください。

### Round 3(BLAKE-{224, 256, 384, 5112})

[blake_ref.c](https://www.aumasson.jp/blake/blake_ref.c)と[blake_ref.h](https://www.aumasson.jp/blake/blake_ref.h)を用意してください。

## BLAKE2

{blake2-impl.h, blake2.h, blake2s-ref.c, blake2sp-ref.c, blake2b-ref.c, blake2bp-ref.c, blake2xs-ref.c, blake2xb-ref.c} are copied from [Here](https://github.com/BLAKE2/BLAKE2)

## EDON-R

[ここ](https://web.archive.org/web/20150906080640/http://csrc.nist.gov/groups/ST/hash/sha-3/Round1/documents/EDON-R.zip)からパッケージをダウンロードして、`Edon-R-ref.c`と`SHA3api_ref.h`を用意してください。

## Keccak

Depends on [XKCP](https://github.com/XKCP/XKCP)

## MD2

md2.c, md2.h and global.h are copied from [RFC1319](https://tools.ietf.org/html/rfc1319)

## MD4

md4.c, md4.h and global.h are copied from [RFC1320](https://tools.ietf.org/html/rfc1320)

## MD5

md5.c, md5.h and global.h are copied from [RFC1321](https://tools.ietf.org/html/rfc1321)

## RIPEMD-{128, 160, 256, 320}

Depends on Crypto++(libcryptopp.so, cryptopp/ripemd.h)

## SHA-0

sha0.c(renamed from sha1.c) and sha1.h are copied from [RFC3174](https://tools.ietf.org/html/rfc3174)

Replace
`W[t] = SHA1CircularShift(1, W[t - 3] ^ W[t - 8] ^ W[t - 14] ^ W[t - 16]);`
to
`W[t] = W[t - 3] ^ W[t - 8] ^ W[t - 14] ^ W[t - 16];`

and rename sha1{.c, .h} to sha0{.c, .h}

## SHA-1

sha1.c and sha1.h are copied from [RFC3174](https://tools.ietf.org/html/rfc3174)

## SHA-2

Depends on OpenSSL(libcrypto.so, evp.h, sha.h)

## SHA-3

Depends on [XKCP](https://github.com/XKCP/XKCP)
