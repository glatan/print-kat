#include "type.h"

// BLAKE (Round2)
void blake28(size_t message_len, const BitSequence *message, BitSequence *digest);
void blake32(size_t message_len, const BitSequence *message, BitSequence *digest);
void blake48(size_t message_len, const BitSequence *message, BitSequence *digest);
void blake64(size_t message_len, const BitSequence *message, BitSequence *digest);

// BLAKE (Round3)
void blake224(size_t message_len, const BitSequence *message, BitSequence *digest);
void blake256(size_t message_len, const BitSequence *message, BitSequence *digest);
void blake384(size_t message_len, const BitSequence *message, BitSequence *digest);
void blake512(size_t message_len, const BitSequence *message, BitSequence *digest);

// BLAKE2
void _blake2s(size_t message_len, size_t digest_len, const uint8_t *message, uint8_t *digest);
void blake2s_with_key(size_t message_len, const uint8_t *message, uint8_t *digest);
void _blake2b(size_t message_len, size_t digest_len, const uint8_t *message, uint8_t *digest);
void blake2b_with_key(size_t message_len, const uint8_t *message, uint8_t *digest);

// EDON-R
void edonr224(size_t message_len, const BitSequence *message, BitSequence *digest);
void edonr256(size_t message_len, const BitSequence *message, BitSequence *digest);
void edonr384(size_t message_len, const BitSequence *message, BitSequence *digest);
void edonr512(size_t message_len, const BitSequence *message, BitSequence *digest);

// Groestl
void groestl224(size_t message_len, const BitSequence *message, BitSequence *digest);
void groestl256(size_t message_len, const BitSequence *message, BitSequence *digest);
void groestl384(size_t message_len, const BitSequence *message, BitSequence *digest);
void groestl512(size_t message_len, const BitSequence *message, BitSequence *digest);

// Keccak(Not SHA-3)
void keccak_r40c160(size_t message_len, const BitSequence *message, BitSequence *digest);
void keccak_r144c256(size_t message_len, const BitSequence *message, BitSequence *digest);
void keccak_r240c160(size_t message_len, const BitSequence *message, BitSequence *digest);
void keccak_r288c512(size_t message_len, const BitSequence *message, BitSequence *digest);
void keccak_r544c256(size_t message_len, const BitSequence *message, BitSequence *digest);
void keccak_r640c160(size_t message_len, const BitSequence *message, BitSequence *digest);
void keccak224(size_t message_len, const BitSequence *message, BitSequence *digest);
void keccak256(size_t message_len, const BitSequence *message, BitSequence *digest);
void keccak384(size_t message_len, const BitSequence *message, BitSequence *digest);
void keccak512(size_t message_len, const BitSequence *message, BitSequence *digest);

// MD2
void md2(size_t message_len, const uint8_t *message, uint8_t *digest);

// MD4
void md4(size_t message_len, const uint8_t *message, uint8_t *digest);

// MD5
void md5(size_t message_len, const uint8_t *message, uint8_t *digest);

// RIPEMD-{128, 160, 256, 320}
void ripemd128(size_t message_len, const uint8_t *message, uint8_t *digest);
void ripemd160(size_t message_len, const uint8_t *message, uint8_t *digest);
void ripemd256(size_t message_len, const uint8_t *message, uint8_t *digest);
void ripemd320(size_t message_len, const uint8_t *message, uint8_t *digest);

// SHA-0
void sha0(size_t message_len, const uint8_t *message, uint8_t *digest);

// SHA-1
void sha1(size_t message_len, const uint8_t *message, uint8_t *digest);

// SHA-2
void sha224(size_t message_len, const uint8_t *message, uint8_t *digest);
void sha256(size_t message_len, const uint8_t *message, uint8_t *digest);
void sha384(size_t message_len, const uint8_t *message, uint8_t *digest);
void sha512(size_t message_len, const uint8_t *message, uint8_t *digest);
void sha512_224(size_t message_len, const uint8_t *message, uint8_t *digest);
void sha512_256(size_t message_len, const uint8_t *message, uint8_t *digest);

// SHA-3
void sha3_224(size_t message_len, const BitSequence *message, BitSequence *digest);
void sha3_256(size_t message_len, const BitSequence *message, BitSequence *digest);
void sha3_384(size_t message_len, const BitSequence *message, BitSequence *digest);
void sha3_512(size_t message_len, const BitSequence *message, BitSequence *digest);
void shake128(size_t message_len, size_t digest_len, const BitSequence *message, uint8_t *digest);
void shake256(size_t message_len, size_t digest_len, const BitSequence *message, uint8_t *digest);
