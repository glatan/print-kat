#include "type.h"

void Print_KAT_Bytes(Algorithm algorithm, size_t digest_len);
void Print_KAT_Bytes_XOF(XOFAlgorithm algorithm, size_t digest_len);
void Print_KAT_BitSequence(Algorithm algorithm, size_t digest_len);
void Print_KAT_BitSequence_XOF(XOFAlgorithm algorithm, size_t digest_len);
