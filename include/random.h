#include "type.h"

void random_uint8t(uint8_t *buffer, size_t size);
void random_bitsequence(BitSequence *buffer, size_t size);
