#include <stdint.h>
#include <stdlib.h>

typedef unsigned char BitSequence;

typedef void (*Algorithm)(size_t message_len, const uint8_t *message, uint8_t *digest);
typedef void (*XOFAlgorithm)(size_t message_len, size_t digest_len, const uint8_t *message, uint8_t *digest);
