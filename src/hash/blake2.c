#include "../../include/hash.h"
#include "../../include/random.h"

#include "../../third_party/blake2/blake2.h"

void _blake2s(size_t message_len, size_t digest_len, const uint8_t *message, uint8_t *digest)
{
    blake2s(digest, digest_len, message, message_len, NULL, 0);
}

void blake2s_with_key(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    uint8_t key[32];
    random_uint8t(key, 32);
    blake2s(digest, 16, message, message_len, key, 32);
}

void _blake2b(size_t message_len, size_t digest_len, const uint8_t *message, uint8_t *digest)
{
    blake2b(digest, digest_len, message, message_len, NULL, 0);
}

void blake2b_with_key(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    uint8_t key[32];
    random_uint8t(key, 32);
    blake2b(digest, 20, message, message_len, key, 32);
}
