#include "../../include/hash.h"

#include "../../third_party/blake/blake_ref_round2.h"

void blake28(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    BLAKE_Round2_Hash(224, message, message_len, digest);
}

void blake32(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    BLAKE_Round2_Hash(256, message, message_len, digest);
}

void blake48(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    BLAKE_Round2_Hash(384, message, message_len, digest);
}

void blake64(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    BLAKE_Round2_Hash(512, message, message_len, digest);
}
