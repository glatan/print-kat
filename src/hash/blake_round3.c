#include "../../include/hash.h"

#include "../../third_party/blake/blake_ref_round3.h"

void blake224(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    BLAKE_Round3_Hash(224, message, message_len, digest);
}

void blake256(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    BLAKE_Round3_Hash(256, message, message_len, digest);
}

void blake384(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    BLAKE_Round3_Hash(384, message, message_len, digest);
}

void blake512(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    BLAKE_Round3_Hash(512, message, message_len, digest);
}
