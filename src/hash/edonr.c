#include "../../include/hash.h"

#include "../../third_party/edonr/SHA3api_ref.h"

void edonr224(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    EdonR_Hash(224, message, message_len, digest);
}

void edonr256(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    EdonR_Hash(256, message, message_len, digest);
}

void edonr384(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    EdonR_Hash(384, message, message_len, digest);
}

void edonr512(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    EdonR_Hash(512, message, message_len, digest);
}
