#include "../../include/hash.h"

#include "../../third_party/groestl/Groestl-ref.h"

void groestl224(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    Groestl_Hash(224, message, message_len, digest);
}

void groestl256(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    Groestl_Hash(256, message, message_len, digest);
}

void groestl384(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    Groestl_Hash(384, message, message_len, digest);
}

void groestl512(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    Groestl_Hash(512, message, message_len, digest);
}
