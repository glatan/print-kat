#include <xkcp/KeccakHash.h>
#include <xkcp/KeccakSponge.h>

#include "../../include/hash.h"

void keccak_r40c160(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    KeccakWidth200_Sponge(40, 160, message, message_len, 0x01, digest, 512);
}

void keccak_r144c256(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    KeccakWidth400_Sponge(144, 256, message, message_len, 0x01, digest, 512);
}

void keccak_r240c160(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    KeccakWidth400_Sponge(240, 160, message, message_len, 0x01, digest, 512);
}

void keccak_r288c512(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    KeccakWidth800_Sponge(288, 512, message, message_len, 0x01, digest, 512);
}

void keccak_r544c256(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    KeccakWidth800_Sponge(544, 256, message, message_len, 0x01, digest, 512);
}

void keccak_r640c160(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    KeccakWidth800_Sponge(640, 160, message, message_len, 0x01, digest, 512);
}

void keccak224(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    Keccak_HashInstance ctx;
    Keccak_HashInitialize(&ctx, 1152, 1600 - 1152, 224, 0x01);
    Keccak_HashUpdate(&ctx, message, message_len);
    Keccak_HashFinal(&ctx, digest);
}

void keccak256(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    Keccak_HashInstance ctx;
    Keccak_HashInitialize(&ctx, 1088, 1600 - 1088, 256, 0x01);
    Keccak_HashUpdate(&ctx, message, message_len);
    Keccak_HashFinal(&ctx, digest);
}

void keccak384(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    Keccak_HashInstance ctx;
    Keccak_HashInitialize(&ctx, 832, 1600 - 832, 384, 0x01);
    Keccak_HashUpdate(&ctx, message, message_len);
    Keccak_HashFinal(&ctx, digest);
}

void keccak512(size_t message_len, const BitSequence *message, BitSequence *digest)
{
    Keccak_HashInstance ctx;
    Keccak_HashInitialize(&ctx, 576, 1600 - 576, 512, 0x01);
    Keccak_HashUpdate(&ctx, message, message_len);
    Keccak_HashFinal(&ctx, digest);
}
