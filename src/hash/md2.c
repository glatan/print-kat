#include "../../include/hash.h"

#include "../../third_party/md2/global.h"
#include "../../third_party/md2/md2.h"

void md2(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    MD2_CTX ctx;
    MD2Init(&ctx);
    MD2Update(&ctx, message, message_len);
    MD2Final(digest, &ctx);
}
