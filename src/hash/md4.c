#include "../../include/hash.h"

#include "../../third_party/md2/global.h"
#include "../../third_party/md4/md4.h"

void md4(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    MD4_CTX ctx;
    MD4Init(&ctx);
    MD4Update(&ctx, message, message_len);
    MD4Final(digest, &ctx);
}
