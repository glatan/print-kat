#include "../../include/hash.h"

#include "../../third_party/md2/global.h"
#include "../../third_party/md5/md5.h"

void md5(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    MD5_CTX ctx;
    MD5Init(&ctx);
    MD5Update(&ctx, message, message_len);
    MD5Final(digest, &ctx);
}
