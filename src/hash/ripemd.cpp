#include <cryptopp/ripemd.h>

using namespace CryptoPP;

extern "C" void ripemd128(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    RIPEMD128 hash;
    hash.Update(message, message_len);
    hash.Final(digest);
}

extern "C" void ripemd160(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    RIPEMD160 hash;
    hash.Update(message, message_len);
    hash.Final(digest);
}

extern "C" void ripemd256(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    RIPEMD256 hash;
    hash.Update(message, message_len);
    hash.Final(digest);
}

extern "C" void ripemd320(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    RIPEMD320 hash;
    hash.Update(message, message_len);
    hash.Final(digest);
}
