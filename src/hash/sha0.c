#include "../../include/hash.h"

#include "../../third_party/sha0/sha0.h"

void sha0(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    SHA0Context ctx;
    SHA0Reset(&ctx);
    SHA0Input(&ctx, &message[0], message_len);
    SHA0Result(&ctx, digest);
}
