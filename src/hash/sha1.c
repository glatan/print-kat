#include "../../include/hash.h"

#include "../../third_party/sha1/sha1.h"

void sha1(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    SHA1Context ctx;
    SHA1Reset(&ctx);
    SHA1Input(&ctx, &message[0], message_len);
    SHA1Result(&ctx, digest);
}
