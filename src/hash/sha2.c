#include "../../include/hash.h"

#include <openssl/sha.h>
#include <openssl/evp.h>

void sha224(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    SHA256_CTX ctx;
    SHA224_Init(&ctx);
    SHA224_Update(&ctx, message, message_len);
    SHA224_Final(digest, &ctx);
}

void sha256(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    SHA256_CTX ctx;
    SHA256_Init(&ctx);
    SHA256_Update(&ctx, message, message_len);
    SHA256_Final(digest, &ctx);
}

void sha384(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    SHA512_CTX ctx;
    SHA384_Init(&ctx);
    SHA384_Update(&ctx, message, message_len);
    SHA384_Final(digest, &ctx);
}

void sha512(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    SHA512_CTX ctx;
    SHA512_Init(&ctx);
    SHA512_Update(&ctx, message, message_len);
    SHA512_Final(digest, &ctx);
}

void sha512_224(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    EVP_MD_CTX *ctx = EVP_MD_CTX_create();
    const EVP_MD *md = EVP_get_digestbyname("sha512-224");
    EVP_DigestInit_ex(ctx, md, NULL);
    EVP_DigestUpdate(ctx, message, message_len);
    EVP_DigestFinal_ex(ctx, (unsigned char *)digest, NULL);
    EVP_MD_CTX_free(ctx);
}

void sha512_256(size_t message_len, const uint8_t *message, uint8_t *digest)
{
    EVP_MD_CTX *ctx = EVP_MD_CTX_create();
    const EVP_MD *md = EVP_get_digestbyname("sha512-256");
    EVP_DigestInit_ex(ctx, md, NULL);
    EVP_DigestUpdate(ctx, message, message_len);
    EVP_DigestFinal_ex(ctx, (unsigned char *)digest, NULL);
    EVP_MD_CTX_free(ctx);
}
