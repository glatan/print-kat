#include <stdio.h>
#include <string.h>

#include "../include/kat.h"
#include "../include/random.h"

#define LIMIT 255

void Print_KAT_Bytes(Algorithm algorithm, size_t digest_len)
{
    uint8_t message[LIMIT];
    uint8_t digest[digest_len];

    puts("const SHORT_MSG_KAT: [(&str, &str); 255] = [");
    for (size_t message_length = 0; message_length < LIMIT; message_length++)
    {
        random_uint8t(message, message_length);
        algorithm(message_length, message, digest);
        printf("\t(\n\t\t");
        printf("// Len = %lu\n\t\t\"", message_length * 8);
        for (size_t j = 0; j < message_length; j++)
        {
            printf("%02x", message[j]);
        }
        printf("\",\n\t\t\"");
        for (size_t j = 0; j < digest_len; j++)
        {
            printf("%02x", digest[j]);
        }
        puts("\"\n\t),");
    }
    puts("];");
}

void Print_KAT_Bytes_XOF(XOFAlgorithm algorithm, size_t digest_len)
{
    uint8_t message[LIMIT];
    uint8_t digest[digest_len];

    puts("const SHORT_MSG_KAT: [(&str, &str); 255] = [");
    for (size_t message_length = 0; message_length < LIMIT; message_length++)
    {
        random_uint8t(message, message_length);
        (*algorithm)(message_length, digest_len, message, digest);
        printf("\t(\n\t\t");
        printf("// Len = %lu\n\t\t\"", message_length * 8);
        for (size_t j = 0; j < message_length; j++)
        {
            printf("%02x", message[j]);
        }
        printf("\",\n\t\t\"");
        for (size_t j = 0; j < digest_len; j++)
        {
            printf("%02x", digest[j]);
        }
        puts("\"\n\t),");
    }
    puts("];");
}

void Print_KAT_BitSequence(Algorithm algorithm, size_t digest_len)
{
    BitSequence message[LIMIT * 8];
    BitSequence digest[digest_len * 8];
    puts("const SHORT_MSG_KAT: [(&str, &str); 255] = [");
    for (size_t message_length = 0; message_length < LIMIT * 8; message_length += 8)
    {
        random_bitsequence(message, message_length / 8);
        (*algorithm)(message_length, message, digest);
        printf("\t(\n\t\t");
        printf("// Len = %zu\n\t\t\"", message_length);
        for (size_t j = 0; j < message_length / 8; j++)
        {
            printf("%02x", message[j]);
        }
        printf("\",\n\t\t\"");
        for (size_t j = 0; j < digest_len; j++)
        {
            printf("%02x", digest[j]);
        }
        puts("\"\n\t),");
    }
    puts("];");
}

void Print_KAT_BitSequence_XOF(XOFAlgorithm algorithm, size_t digest_len)
{
    BitSequence message[LIMIT * 8];
    BitSequence digest[digest_len * 8];
    puts("const SHORT_MSG_KAT: [(&str, &str); 255] = [");
    for (size_t message_length = 0; message_length < LIMIT * 8; message_length += 8)
    {
        random_bitsequence(message, message_length / 8);
        (*algorithm)(message_length, digest_len, message, digest);
        printf("\t(\n\t\t");
        printf("// Len = %zu\n\t\t\"", message_length);
        for (size_t j = 0; j < message_length / 8; j++)
        {
            printf("%02x", message[j]);
        }
        printf("\",\n\t\t\"");
        for (size_t j = 0; j < digest_len; j++)
        {
            printf("%02x", digest[j]);
        }
        puts("\"\n\t),");
    }
    puts("];");
}
