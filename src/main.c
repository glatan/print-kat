#include <stdio.h>
#include <string.h>

#include "../include/kat.h"
#include "../include/hash.h"

// example: print_kat blake224
// example: print-kat md5
int main(int argc, char **argv)
{
    if (argc < 2)
    {
        puts("Please select hash algorithm.");
        return 1;
    }
    else
    {
        size_t digest_len;
        if (argc == 3)
        {
            digest_len = atoi(argv[2]);
        }

        if (strcmp(argv[1], "blake28") == 0)
        {
            Algorithm algorithm = blake28;
            Print_KAT_BitSequence(algorithm, 28);
        }
        if (strcmp(argv[1], "blake32") == 0)
        {
            Algorithm algorithm = blake32;
            Print_KAT_BitSequence(algorithm, 32);
        }
        if (strcmp(argv[1], "blake48") == 0)
        {
            Algorithm algorithm = blake48;
            Print_KAT_BitSequence(algorithm, 48);
        }
        if (strcmp(argv[1], "blake64") == 0)
        {
            Algorithm algorithm = blake64;
            Print_KAT_BitSequence(algorithm, 64);
        }
        if (strcmp(argv[1], "blake224") == 0)
        {
            Algorithm algorithm = blake224;
            Print_KAT_BitSequence(algorithm, 28);
        }
        if (strcmp(argv[1], "blake256") == 0)
        {
            Algorithm algorithm = blake256;
            Print_KAT_BitSequence(algorithm, 32);
        }
        if (strcmp(argv[1], "blake384") == 0)
        {
            Algorithm algorithm = blake384;
            Print_KAT_BitSequence(algorithm, 48);
        }
        if (strcmp(argv[1], "blake512") == 0)
        {
            Algorithm algorithm = blake512;
            Print_KAT_BitSequence(algorithm, 64);
        }
        if (strcmp(argv[1], "blake2s") == 0)
        {
            XOFAlgorithm algorithm = _blake2s;
            Print_KAT_Bytes_XOF(algorithm, digest_len);
        }
        if (strcmp(argv[1], "blake2s_with_key") == 0)
        {
            Algorithm algorithm = blake2s_with_key;
            Print_KAT_Bytes(algorithm, 16);
        }
        if (strcmp(argv[1], "blake2b") == 0)
        {
            XOFAlgorithm algorithm = _blake2b;
            Print_KAT_Bytes_XOF(algorithm, digest_len);
        }
        if (strcmp(argv[1], "blake2b_with_key") == 0)
        {
            Algorithm algorithm = blake2b_with_key;
            Print_KAT_Bytes(algorithm, 20);
        }
        if (strcmp(argv[1], "edonr224") == 0)
        {
            Algorithm algorithm = edonr224;
            Print_KAT_BitSequence(algorithm, 28);
        }
        if (strcmp(argv[1], "edonr256") == 0)
        {
            Algorithm algorithm = edonr256;
            Print_KAT_BitSequence(algorithm, 32);
        }
        if (strcmp(argv[1], "edonr384") == 0)
        {
            Algorithm algorithm = edonr384;
            Print_KAT_BitSequence(algorithm, 48);
        }
        if (strcmp(argv[1], "edonr512") == 0)
        {
            Algorithm algorithm = edonr512;
            Print_KAT_BitSequence(algorithm, 64);
        }
        if (strcmp(argv[1], "groestl224") == 0)
        {
            Algorithm algorithm = groestl224;
            Print_KAT_BitSequence(algorithm, 28);
        }
        if (strcmp(argv[1], "groestl256") == 0)
        {
            Algorithm algorithm = groestl256;
            Print_KAT_BitSequence(algorithm, 32);
        }
        if (strcmp(argv[1], "groestl384") == 0)
        {
            Algorithm algorithm = groestl384;
            Print_KAT_BitSequence(algorithm, 48);
        }
        if (strcmp(argv[1], "groestl512") == 0)
        {
            Algorithm algorithm = groestl512;
            Print_KAT_BitSequence(algorithm, 64);
        }
        if (strcmp(argv[1], "keccak_r40c160") == 0)
        {
            Algorithm algorithm = keccak_r40c160;
            Print_KAT_Bytes(algorithm, 512);
        }
        if (strcmp(argv[1], "keccak_r144c256") == 0)
        {
            Algorithm algorithm = keccak_r144c256;
            Print_KAT_Bytes(algorithm, 512);
        }
        if (strcmp(argv[1], "keccak_r240c160") == 0)
        {
            Algorithm algorithm = keccak_r240c160;
            Print_KAT_Bytes(algorithm, 512);
        }
        if (strcmp(argv[1], "keccak_r288c512") == 0)
        {
            Algorithm algorithm = keccak_r288c512;
            Print_KAT_Bytes(algorithm, 512);
        }
        if (strcmp(argv[1], "keccak_r544c256") == 0)
        {
            Algorithm algorithm = keccak_r544c256;
            Print_KAT_Bytes(algorithm, 512);
        }
        if (strcmp(argv[1], "keccak_r640c160") == 0)
        {
            Algorithm algorithm = keccak_r640c160;
            Print_KAT_Bytes(algorithm, 512);
        }
        if (strcmp(argv[1], "keccak224") == 0)
        {
            Algorithm algorithm = keccak224;
            Print_KAT_BitSequence(algorithm, 28);
        }
        if (strcmp(argv[1], "keccak256") == 0)
        {
            Algorithm algorithm = keccak256;
            Print_KAT_BitSequence(algorithm, 32);
        }
        if (strcmp(argv[1], "keccak384") == 0)
        {
            Algorithm algorithm = keccak384;
            Print_KAT_BitSequence(algorithm, 48);
        }
        if (strcmp(argv[1], "keccak512") == 0)
        {
            Algorithm algorithm = keccak512;
            Print_KAT_BitSequence(algorithm, 64);
        }
        if (strcmp(argv[1], "md2") == 0)
        {
            Algorithm algorithm = md2;
            Print_KAT_Bytes(algorithm, 16);
        }
        if (strcmp(argv[1], "md4") == 0)
        {
            Algorithm algorithm = md4;
            Print_KAT_Bytes(algorithm, 16);
        }
        if (strcmp(argv[1], "md5") == 0)
        {
            Algorithm algorithm = md5;
            Print_KAT_Bytes(algorithm, 16);
        }
        if (strcmp(argv[1], "ripemd128") == 0)
        {
            Algorithm algorithm = ripemd128;
            Print_KAT_Bytes(algorithm, 16);
        }
        if (strcmp(argv[1], "ripemd160") == 0)
        {
            Algorithm algorithm = ripemd160;
            Print_KAT_Bytes(algorithm, 20);
        }
        if (strcmp(argv[1], "ripemd256") == 0)
        {
            Algorithm algorithm = ripemd256;
            Print_KAT_Bytes(algorithm, 32);
        }
        if (strcmp(argv[1], "ripemd320") == 0)
        {
            Algorithm algorithm = ripemd320;
            Print_KAT_Bytes(algorithm, 40);
        }
        if (strcmp(argv[1], "sha0") == 0)
        {
            Algorithm algorithm = sha0;
            Print_KAT_Bytes(algorithm, 20);
        }
        if (strcmp(argv[1], "sha1") == 0)
        {
            Algorithm algorithm = sha1;
            Print_KAT_Bytes(algorithm, 20);
        }
        if (strcmp(argv[1], "sha224") == 0)
        {
            Algorithm algorithm = sha224;
            Print_KAT_Bytes(algorithm, 28);
        }
        if (strcmp(argv[1], "sha256") == 0)
        {
            Algorithm algorithm = sha256;
            Print_KAT_Bytes(algorithm, 32);
        }
        if (strcmp(argv[1], "sha384") == 0)
        {
            Algorithm algorithm = sha384;
            Print_KAT_Bytes(algorithm, 48);
        }
        if (strcmp(argv[1], "sha512") == 0)
        {
            Algorithm algorithm = sha512;
            Print_KAT_Bytes(algorithm, 64);
        }
        if (strcmp(argv[1], "sha512/224") == 0)
        {
            Algorithm algorithm = sha512_224;
            Print_KAT_Bytes(algorithm, 28);
        }
        if (strcmp(argv[1], "sha512/256") == 0)
        {
            Algorithm algorithm = sha512_256;
            Print_KAT_Bytes(algorithm, 32);
        }
        if (strcmp(argv[1], "sha3-224") == 0)
        {
            Algorithm algorithm = sha3_224;
            Print_KAT_BitSequence(algorithm, 28);
        }
        if (strcmp(argv[1], "sha3-256") == 0)
        {
            Algorithm algorithm = sha3_256;
            Print_KAT_BitSequence(algorithm, 32);
        }
        if (strcmp(argv[1], "sha3-384") == 0)
        {
            Algorithm algorithm = sha3_384;
            Print_KAT_BitSequence(algorithm, 48);
        }
        if (strcmp(argv[1], "sha3-512") == 0)
        {
            Algorithm algorithm = sha3_512;
            Print_KAT_BitSequence(algorithm, 64);
        }
        if (strcmp(argv[1], "shake128") == 0)
        {
            XOFAlgorithm algorithm = shake128;
            Print_KAT_BitSequence_XOF(algorithm, digest_len);
        }
        if (strcmp(argv[1], "shake256") == 0)
        {
            XOFAlgorithm algorithm = shake256;
            Print_KAT_BitSequence_XOF(algorithm, digest_len);
        }
    }

    return 0;
}
