#include <time.h>
#include<sys/random.h>

#include "../include/random.h"

void random_uint8t(uint8_t *buffer, size_t size)
{
    for (size_t i = 0; i < size; i++)
    {
        getentropy(&buffer[i], 1);
    }
}

void random_bitsequence(BitSequence *buffer, size_t size)
{
    for (size_t i = 0; i < size; i++)
    {
        getentropy(&buffer[i], 1);
    }
}
